/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

Vue.component('admin-booking-form', require('./components/AdminBookingForm.vue').default);
Vue.component('address-search', require('./components/AddressSearch.vue').default);
Vue.component('alert', require('./components/Alert.vue').default);
Vue.component('booking-form', require('./components/BookingForm.vue').default);
Vue.component('checkout-button', require('./components/CheckoutButton.vue').default);
Vue.component('dependent', require('./components/Dependent.vue').default);
Vue.component('faqs', require('./components/FAQs.vue').default);
Vue.component('flatpickr', require('./components/Flatpickr.vue').default);
Vue.component('home-hero', require('./components/HomeHero.vue').default);
Vue.component('ride-payment-form', require('./components/RidePaymentForm.vue').default);
Vue.component('scroll-anchor', require('./components/ScrollAnchor.vue').default);
Vue.component('unsplash-photo-credits', require('./components/UnsplashPhotoCredits.vue').default);

import { Datetime } from 'vue-datetime';

Vue.component('datetime', Datetime);

window.eventHub = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#app'
});
