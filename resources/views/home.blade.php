@extends('layouts.app')

@section('head')
    <meta name="keywords" content="park city transportation,park city transit,transportation,park city,transport,transit,airport,taxi,taxis,shuttle,resort,deer valley,summit,salt lake,slc,salt lake city,pc">
    <meta name="description" content="SummiTrans is the most straight-forward way to hitch a ride in Park City and/or Salt Lake City.">

    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.maps.key') }}&libraries=places"></script>
@endsection

@section('content')
    <article class="home-page">
        <home-hero inline-template>
            <section class="hero">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="overlay">
                                <h1>Park City and Salt Lake City Transportation</h1>

                                <h2>In-Town and Airport Transit</h2>

                                <p>SummiTrans is the most straight-forward way to hitch a ride in Park City. Need a ride around town? How about a ride to and from the airport? We've got you covered.</p>
                            </div>
                        </div>

                        <div class="col-12 col-lg-6">
                            <div class="overlay">
                                <h2>Book a Ride Now</h2>

                                <booking-form v-cloak :black-out-dates="{{ $blackOutDates }}"></booking-form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="scroll-indicator" @click="scrollDown">
                    <em>Scroll to continue</em>

                    <i class="fa fa-angle-double-down"></i>
                </div>
            </section>
        </home-hero>

        <section class="pricing">
            <a name="pricing"></a>

            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <span class="fa-stack home-section-icon">
                            <i class="fa fa-stack-2x fa-circle"></i>
                            <i class="fa fa-stack-1x fa-usd"></i>
                        </span>

                        <h3>Pricing</h3>
                    </div>

                    <div class="col-12 col-lg-8">
                        <p>Price is based on distance traveled, here are estimates based on common routes:</p>
                        
                        <ul>
                            <li>Salt Lake City International Airport to Park City &ndash; &#36;155.00</li>
                            
                            <li>Salt Lake City International Airport to Kamas, Oakley, or Victory Ranch &ndash; &#36;165.00</li>
                            
                            <li>Salt Lake City International Airport to Heber City, Red Ledges, or Midway &ndash; &#36;175.00</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="hours right">
            <a name="hours"></a>

            <div class="container">
                <div class="row">
                    <span class="fa-stack home-section-icon">
                        <i class="fa fa-stack-2x fa-circle"></i>
                        <i class="fa fa-stack-1x fa-clock-o"></i>
                    </span>

                    <div class="col-12 col-lg-4">
                        <h3>Hours</h3>
                    </div>

                    <div class="col-12 col-lg-8">
                        <p>
                            Open: 5 AM<br>
                            Close: 5 PM
                        </p>

                        <p>
                            You may provide up to 1 day notice for arranging transportation.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="location">
            <a name="location"></a>

            <div class="container">
                <div class="row">
                    <span class="fa-stack home-section-icon">
                        <i class="fa fa-stack-2x fa-circle"></i>
                        <i class="fa fa-stack-1x fa-map-o"></i>
                        <i class="fa fa-stack-1x fa-map-marker" style="font-size: 0.4rem;"></i>
                    </span>

                    <div class="col-12 col-lg-4">
                        <h3>Location</h3>
                    </div>

                    <div class="col-12 col-lg-8">
                        <p>We are based in Park City but frequently travel to-and-from Salt Lake City for airport and hotel transit.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="faqs right">
            <a name="faqs"></a>

            <div class="container">
                <div class="row">
                    <span class="fa-stack home-section-icon">
                        <i class="fa fa-stack-2x fa-circle"></i>
                        <i class="fa fa-stack-1x fa-question"></i>
                    </span>

                    <div class="col-12 col-lg-4">
                        <h3>FAQs</h3>
                    </div>

                    <div class="col-12 col-lg-8">
                        <faqs :faqs="[
                            {
                                question: 'How many passengers can you accomodate?',
                                answer: 'Our vehicle can accomodate up to 6 adult passengers, and up to 7 with minimal luggage.'
                            },

                            {
                                question: 'How far do you typically travel?',
                                answer: 'We are willing to travel anywhere in the Salt Lake valley as needed. Price will adjust to accomodate longer trips.'
                            },

                            {
                                question: 'Can I cancel my booking?',
                                answer: 'Yes, if you cancel at least 24 hours before your confirmed time, there will be no charge. If you cancel within 24 hours, there may be a cancellation fee.'
                            }
                        ]"></faqs>
                    </div>
                </div>
            </div>
        </section>

        <section class="contact">
            <a name="contact"></a>

            <div class="container">
                <div class="row">
                    <span class="fa-stack home-section-icon">
                        <i class="fa fa-stack-2x fa-circle"></i>
                        <i class="fa fa-stack-1x fa-phone"></i>
                    </span>

                    <div class="col-12 col-lg-4">
                        <h3>Contact</h3>

                        <p>Use the form or call directly at <a style="color: white; text-decoration: underline;" href="tel:8012434132">(801) 243-4132</a></p>
                    </div>

                    <div class="col-12 col-lg-8">
                        <form class="contact-form" action="{{ route('contact.post') }}" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="name">Name</label>

                                <input id="name" name="name" type="text" value="{{ old('name') }}" class="form-control bg-dark text-white border-dark" placeholder="Your Name">
                            </div>

                            <div class="form-group" style="display: none;">
                                <label for="email">Email</label>

                                <input id="email" name="email" type="email" value="{{ old('email') }}" class="form-control bg-dark text-white border-dark" placeholder="Your Email">
                            </div>

                            <div class="form-group">
                                <label for="real_email">Email</label>

                                <input id="real_email" name="real_email" type="real_email" value="{{ old('real_email') }}" class="form-control bg-dark text-white border-dark" placeholder="Your Email">
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone</label>

                                <input id="phone" name="phone" type="phone" value="{{ old('phone') }}" class="form-control bg-dark text-white border-dark" placeholder="(555) 555-5555">
                            </div>

                            <div class="form-group">
                                <label for="message">Message</label>

                                <textarea id="message" name="message" rows="5" class="form-control bg-dark text-white border-dark">{{ old('message') }}</textarea>
                            </div>

                            <button class="btn btn-outline-light" type="submit">
                                Send Message <i class="fa fa-angle-right"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section class="booking right">
            <a name="booking"></a>

            <div class="container">
                <div class="row">
                    <span class="fa-stack home-section-icon">
                        <i class="fa fa-stack-2x fa-circle"></i>
                        <i class="fa fa-stack-1x fa-calendar-check-o"></i>
                    </span>

                    <div class="col-12 col-lg-4">
                        <h3>Book Now</h3>
                    </div>

                    <div class="col-12 col-lg-8">
                        <booking-form v-cloak></booking-form>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection

@section('scripts')
    <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.google.recaptcha.key') }}"></script>
    <script async defer>
        grecaptcha.ready(function() {
            grecaptcha.execute("{{ config('services.google.recaptcha.key') }}", { action: 'homepage' }).then(function (token) {
                var form = jQuery('.contact-form');
                var input = jQuery('<input name="grecaptcha" type="hidden">');

                form.append(input);
                input.val(token);
            });
        });
    </script>
@endsection
