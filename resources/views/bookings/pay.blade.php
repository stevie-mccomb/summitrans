@extends('layouts.app')

@section('content')
    <article class="bookings-show py-5">
        <section class="container">
            <header class="pb-3">
                <h1>Pay for Ride</h1>
            </header>

            @include('partials.alerts')

            <ride-payment-form :booking="{{ $booking }}">
                Loading...
            </ride-payment-form>
        </section>
@endsection
