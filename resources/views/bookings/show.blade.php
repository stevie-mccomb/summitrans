@extends('layouts.app')

@section('content')
    <article class="bookings-show py-5">
        <section class="container">
            <header class="pb-3">
                <h1>
                    Ride

                    @if ($booking->paid)
                        Receipt
                    @elseif ($booking->trashed())
                        Cancelled
                    @elseif ($booking->started)
                        Started
                    @else
                        Confirmed
                    @endif
                </h1>
            </header>

            @include('partials.alerts')

            @if ($booking->paid)
                <p>
                    <em>
                        Your ride has been completed and paid for. Please review your details and receipt below.
                    </em>
                </p>
            @elseif ($booking->trashed())
                <p>
                    <em>
                        Your ride has been cancelled. If you'd like to re-book this reservation, please <a class="text-white" style="text-decoration: underline;" href="{{ route('home') }}#booking">create a new ride here</a>.
                    </em>
                </p>
            @elseif ($booking->started)
                <p>
                    <em>
                        Your ride has begun. You can view the details of your ride below. You will not be charged until the end of your ride, at which time you can add a tip and confirm payment.
                    </em>
                </p>
            @else
                <p>
                    <em>
                        Your ride has been confirmed. You can view the details of your ride below. You will not be charged until the end of your ride, at which time you can add a tip and confirm payment.
                    </em>
                </p>

                <p>
                    <em>
                        Please save this URL and/or reference ID in the event that you need to cancel your ride: <input class="form-control bg-dark border-white text-white" value="{{ url()->current() }}" onclick="jQuery(this).select();" oninput="jQuery(this).val('{{ url()->current() }}')">
                    </em>
                </p>
            @endif

            <table class="booking table bg-dark table-bordered">
                <tbody>
                    @if ($booking->paid)
                        <tr>
                            <th>Total</th>

                            <td>&#36;{{ number_format(round($booking->charge->amount / 100, 2), 2) }}</td>
                        </tr>

                        <tr>
                            <th>Payment Description</th>

                            <td>{{ $booking->charge->description }}</td>
                        </tr>
                    @endif

                    <tr>
                        <th>Reference ID</th>

                        <td>{{ $booking->uid }}</td>
                    </tr>

                    <tr>
                        <th>Name</th>

                        <td>{{ $booking->full_name }}</td>
                    </tr>

                    <tr>
                        <th>Email</th>

                        <td>{{ $booking->email }}</td>
                    </tr>

                    <tr>
                        <th>Phone</th>

                        <td>{{ $booking->phone }}</td>
                    </tr>

                    <tr>
                        <th>Pickup Time</th>

                        <td>{{ $booking->pickup_at->format('g:ia \o\n F jS, Y') }}</td>
                    </tr>

                    <tr>
                        <th>Pickup Location</th>

                        <td>{{ $booking->origin_full_address }}</td>
                    </tr>

                    <tr>
                        <th>Drop-off Location</th>

                        <td>{{ $booking->destination_full_address }}</td>
                    </tr>

                    <tr>
                        <th>Number of Passengers</th>

                        <td>{{ $booking->passengers }} passenger{{ $booking->passengers === 1 ? '' : 's' }}</td>
                    </tr>

                    <tr>
                        <th>Airport Pickup</th>

                        <td>{{ $booking->airport_pickup ? 'Yes' : 'No' }}</td>
                    </tr>

                    @if ($booking->airport_pickup)
                        <tr>
                            <th>Airline</th>

                            <td>{{ $booking->airline }}</td>
                        </tr>

                        <tr>
                            <th>Flight Number</th>

                            <td>{{ $booking->flight_number }}</td>
                        </tr>

                        <tr>
                            <th>Departing City</th>

                            <td>{{ $booking->departing_city }}</td>
                        </tr>
                    @endif

                    <tr>
                        <th>Notes</th>

                        <td>{{ $booking->notes }}</td>
                    </tr>
                </tbody>
            </table>
        </section>

        @if (!$booking->trashed())
            <section class="container">
                @if (!$booking->started && !$booking->paid)
                    <form action="{{ route('bookings.destroy', [ 'uid' => $booking->uid ]) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to cancel your ride?')) return false;">
                        @csrf @method('DELETE')

                        <button type="submit" class="btn btn-primary btn-outline-light">Cancel Ride</button>
                    </form>
                @elseif (Auth::check() && !$booking->paid)
                    <form action="{{ route('bookings.stop', [ 'uid' => $booking->uid ]) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to stop the ride and proceed to payment?')) return false;">
                        @csrf

                        <button type="submit" class="btn btn-outline-primary">
                            <i class="fa fa-credit-card"></i>

                            Stop Ride and Request Payment
                        </button>
                    </form>
                @endif
            </section>
        @endif
    </article>

    @if (!$booking->paid && !$booking->trashed() && !$booking->started)
        <!-- Event snippet for Booking Form conversion page -->
        <script>
            gtag('event', 'conversion', {'send_to': 'AW-936591558/v3oGCKHr_6kBEMaBzb4D'});
        </script>
    @endif
@endsection
