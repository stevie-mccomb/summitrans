@extends('layouts.app')

@section('content')
    <article class="bookings-edit py-5">
        <section class="container">
            <div class="row">
                <div class="col">
                    <header class="pb-3">
                        <h1>Review Booking</h1>
                    </header>

                    @include('partials.alerts')

                    <table class="booking table bg-dark table-bordered">
                        <tbody>
                            <tr>
                                <th>Name</th>

                                <td>{{ $booking->full_name }}</td>
                            </tr>

                            <tr>
                                <th>Email</th>

                                <td>{{ $booking->email }}</td>
                            </tr>

                            <tr>
                                <th>Phone</th>

                                <td>{{ $booking->phone }}</td>
                            </tr>

                            <tr>
                                <th>Pickup Time</th>

                                <td>{{ $booking->pickup_at->format('g:ia \o\n F jS, Y') }}</td>
                            </tr>

                            <tr>
                                <th>Pickup Location</th>

                                <td>{{ $booking->origin_full_address }}</td>
                            </tr>

                            <tr>
                                <th>Drop-off Location</th>

                                <td>{{ $booking->destination_full_address }}</td>
                            </tr>

                            <tr>
                                <th>Number of Passengers</th>

                                <td>{{ $booking->passengers }} passenger{{ $booking->passengers === 1 ? '' : 's' }}</td>
                            </tr>

                            <tr>
                                <th>Airport Pickup</th>

                                <td>{{ $booking->airport_pickup ? 'Yes' : 'No' }}</td>
                            </tr>

                            @if ($booking->airport_pickup)
                                <tr>
                                    <th>Airline</th>

                                    <td>{{ $booking->airline }}</td>
                                </tr>

                                <tr>
                                    <th>Flight Number</th>

                                    <td>{{ $booking->flight_number }}</td>
                                </tr>

                                <tr>
                                    <th>Departing City</th>

                                    <td>{{ $booking->departing_city }}</td>
                                </tr>
                            @endif

                            <tr>
                                <th>Notes</th>

                                <td>{{ $booking->notes }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <form action="{{ route('bookings.update', [ 'uid' => $booking->uid ]) }}" method="POST">
                        <checkout-button class="btn-primary p-3" stripe-key="{{ config('services.stripe.key') }}" description="Card not charged until after ride" email="{{ $booking->email }}" panel-label="Finalize Booking">
                            <i class="fa fa-credit-card"></i> Finalize Booking
                        </checkout-button>
                    </form>

                    <br><br>

                    <p class="smaller">
                        <em>
                            Note that your card will not be charged until after the ride. If you cancel the ride within 24 hours of the scheduled pickup time, you will be charged a cancellation fee.
                        </em>
                    </p>
                </div>
            </div>
        </section>
    </article>
@endsection
