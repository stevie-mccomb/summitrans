@extends('layouts.admin')

@section('head')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places"></script>
@endsection

@section('content')
    <admin-booking-form inline-template v-cloak>
        <form action="{{ $booking->id ? route('admin.bookings.update', $booking) : route('admin.bookings.store') }}" method="POST">
            @csrf @method($booking->id ? 'PATCH' : 'POST')

            <header class="d-flex flex-wrap justify-content-between">
                <h1>{{ $booking->id ? 'Edit' : 'Create' }} Booking</h1>

                <div class="buttons">
                    <a href="{{ route('admin.bookings.emails.notification.resend', $booking) }}">
                        <button class="btn btn-outline-primary" type="button">Re-send Notification Email</button>
                    </a>

                    <button class="btn btn-primary ml-3" type="submit">Save Booking</button>
                </div>
            </header>

            <hr class="border-white">

            @include('partials.alerts')

            @if ($booking->charge_id)
                <div class="form-group">
                    <label for="charge_id">Charge ID</label>

                    <input id="charge_id" type="text" value="{{ $booking->charge_id }}" class="form-control bg-dark text-white" disabled>
                </div>
            @endif

            @if ($booking->customer_id)
                <div class="form-group">
                    <label for="customer_id">Customer ID</label>

                    <input id="customer_id" type="text" value="{{ $booking->customer_id }}" class="form-control bg-dark text-white" disabled>
                </div>
            @endif

            <div class="form-group">
                <label for="reference_id">Reference ID</label>

                <input id="reference_id" type="text" readonly value="{{ $booking->uid }}" class="form-control bg-dark text-white">
            </div>

            <div class="form-group required">
                <label for="first_name">First Name</label>

                <input id="first_name" name="first_name" type="text" value="{{ old('first_name') ?? $booking->first_name }}" class="form-control bg-dark text-white">
            </div>

            <div class="form-group required">
                <label for="last_name">Last Name</label>

                <input id="last_name" name="last_name" type="text" value="{{ old('last_name') ?? $booking->last_name }}" class="form-control bg-dark text-white">
            </div>

            <div class="form-group required">
                <label for="email">Email</label>

                <input id="email" name="email" type="email" value="{{ old('email') ?? $booking->email }}" class="form-control bg-dark text-white">
            </div>

            <div class="form-group required">
                <label for="phone">Phone</label>

                <input id="phone" name="phone" type="text" value="{{ old('phone') ?? $booking->phone }}" class="form-control bg-dark text-white">
            </div>

            <div class="form-group required">
                <label for="pickup_location">Pickup Location</label>

                <address-search id="pickup_location" value="{{ old('pickup_location') ?? $booking->origin_full_address }}" class="form-control bg-dark text-white" v-model="origin"></address-search>

                <input type="hidden" name="pickup_location" type="text">
            </div>

            <div class="form-group">
                <label for="pickup_url">Pickup URL</label>

                <p>
                    <a target="_blank" href="{{ $booking->origin_url }}">
                        {{ $booking->origin_url }}
                    </a>
                </p>
            </div>

            <div class="form-group required">
                <label for="destination_location">Destination Location</label>

                <address-search id="destination_location" value="{{ old('destination_location') ?? $booking->destination_full_address }}" class="form-control bg-dark text-white" v-model="destination"></address-search>

                <input type="hidden" name="destination_location" type="text">
            </div>

            <div class="form-group">
                <label for="destination_url">Destination URL</label>

                <p>
                    <a target="_blank" href="{{ $booking->destination_url }}">
                        {{ $booking->destination_url }}
                    </a>
                </p>
            </div>

            <div class="form-group">
                <label for="number_of_passengers">Number of Passengers</label>

                <select id="number_of_passengers" name="number_of_passengers" class="form-control bg-dark text-white border-dark">
                    <option value="1" {{ $booking->passengers === '1' ? 'selected' : '' }}>1 passenger</option>

                    <option value="2" {{ $booking->passengers === '2' ? 'selected' : '' }}>2 passengers</option>

                    <option value="3" {{ $booking->passengers === '3' ? 'selected' : '' }}>3 passengers</option>

                    <option value="4" {{ $booking->passengers === '4' ? 'selected' : '' }}>4 passengers</option>

                    <option value="5" {{ $booking->passengers === '5' ? 'selected' : '' }}>5 passengers</option>

                    <option value="6" {{ $booking->passengers === '6' ? 'selected' : '' }}>6 passengers</option>

                    <option value="7" {{ $booking->passengers === '7' ? 'selected' : '' }}>7 passengers (with minimal luggage)</option>
                </select>
            </div>

            <div class="form-group required">
                <label for="airport_pickup">Airport Pickup</label>

                @php $oldAirport = old('airport_pickup') ? old('airport_pickup') == 'true' : $booking->airport_pickup; @endphp

                <select id="airport_pickup" name="airport_pickup" class="form-control bg-dark text-white">
                    <option value="false" {{ $oldAirport ? 'selected' : '' }}>No</option>

                    <option value="true" {{ $oldAirport ? 'selected' : '' }}>Yes</option>
                </select>
            </div>

            <dependent on="airport_pickup" value="true" class="form-group required" v-cloak>
                <label for="airline">Airline</label>

                <input id="airline" name="airline" type="text" value="{{ old('airline') ?? $booking->airline }}" class="form-control bg-dark text-white">
            </dependent>

            <dependent on="airport_pickup" value="true" class="form-group required" v-cloak>
                <label for="flight_number">Flight Number</label>

                <input id="flight_number" name="flight_number" type="text" value="{{ old('flight_number') ?? $booking->flight_number }}" class="form-control bg-dark text-white">
            </dependent>

            <dependent on="airport_pickup" value="true" class="form-group required" v-cloak>
                <label for="departing_city">Departing City</label>

                <input id="departing_city" name="departing_city" type="text" value="{{ old('departing_city') ?? $booking->departing_city }}" class="form-control bg-dark text-white">
            </dependent>

            <div class="form-group">
                <label for="notes">Notes</label>

                <textarea id="notes" name="notes" rows="5" class="form-control bg-dark text-white">{{ old('notes') ?? $booking->notes }}</textarea>
            </div>

            <button class="btn btn-primary" type="submit">Save Booking</button>
        </form>
    </admin-booking-form>
@endsection
