@extends('layouts.admin')

@section('content')
    <header class="d-flex flex-wrap justify-content-between">
        <h1>Bookings</h1>
    </header>

    @include('partials.alerts')

    <table class="table table-striped bg-dark">
        <thead>
            <tr>
                <th>
                    Status
                </th>

                <th>
                    Customer
                </th>

                <th>
                    Pickup At
                </th>

                <th class="fit"></th>

                <th class="fit"></th>

                <th class="fit"></th>
            </tr>
        </thead>

        <tbody>
            @if ($bookings->count())
                @foreach ($bookings as $booking)
                    <tr>
                        <td>
                            @if ($booking->paid)
                                <span class="badge badge-success">Paid</span>
                            @elseif ($booking->trashed())
                                <span class="badge badge-danger">Cancelled</span>
                            @elseif ($booking->stopped)
                                <span class="badge badge-secondary">Stopped</span>
                            @elseif ($booking->started)
                                <span class="badge badge-warning">In Progress</span>
                            @elseif ($booking->abandoned)
                                <span class="badge badge-secondary">Abandoned</span>
                            @else
                                <span class="badge badge-primary">Booked</span>
                            @endif
                        </td>

                        <td>
                            {{ $booking->first_name }} {{ $booking->last_name }} &lt;{{ $booking->email }}&gt;
                        </td>

                        <td>
                            @if ($booking->pickup_at >= today() && $booking->pickup_at < tomorrow())
                                {{ $booking->pickup_at->format('g:ia') }} Today
                            @elseif ($booking->pickup_at >= tomorrow() && $booking->pickup_at < dayAfterTomorrow())
                                {{ $booking->pickup_at->format('g:ia') }} Tomorrow
                            @else
                                {{ $booking->pickup_at->format('g:ia T \o\n F jS, Y') }}
                            @endif
                        </td>

                        <td class="fit">
                            @if (!$booking->abandoned && !$booking->trashed())
                                @if (!$booking->started && !$booking->paid)
                                    <form action="{{ route('admin.bookings.start', $booking) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to start the ride?')) return false;">
                                        @csrf @method('PATCH')

                                        <button class="btn btn-outline-success" type="submit" title="Start Ride">
                                            Start Ride
                                        </button>
                                    </form>
                                @else
                                    <a href="{{ route('bookings.show', [ 'uid' => $booking->uid ]) }}">
                                        <button class="btn btn-outline-success" type="button">
                                            View {{ $booking->paid ? 'Receipt' : 'Ride' }}
                                        </button>
                                    </a>
                                @endif
                            @endif
                        </td>

                        <td class="fit">
                            @if (!$booking->trashed())
                                <a href="{{ route('admin.bookings.edit', $booking) }}" title="Edit Booking">
                                    <button class="btn btn-primary" type="button">
                                        Edit
                                    </button>
                                </a>
                            @else
                                <a target="_blank" href="{{ route('bookings.show', [ 'uid' => $booking->uid ]) }}" title="View Booking">
                                    <button class="btn btn-outline-light" type="button">
                                        View
                                    </button>
                                </a>
                            @endif
                        </td>

                        <td class="fit">
                            @if (!$booking->abandoned)
                                @if (!$booking->trashed())
                                    <form action="{{ route('admin.bookings.destroy', $booking) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to cancel this booking? The individual that created the booking will receive a cancellation notice email.')) return false;">
                                        @csrf @method('DELETE')

                                        <button class="btn btn-outline-danger" type="submit" title="Cancel Booking">
                                            Cancel
                                        </button>
                                    </form>
                                @else
                                    <form action="{{ route('admin.bookings.restore', $booking) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to restore this booking? The individual that created the booking will receive a restoration notice email.')) return false;">
                                        @csrf @method('PATCH')

                                        <button class="btn btn-outline-success" type="submit" title="Restore Booking">
                                            Restore
                                        </button>
                                    </form>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4">
                        There are currently no bookings matching your criteria.
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
@endsection
