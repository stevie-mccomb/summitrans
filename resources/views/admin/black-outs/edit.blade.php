@extends('layouts.admin')

@section('content')
    <form action="{{ $blackOut->id ? route('admin.black-outs.update', $blackOut) : route('admin.black-outs.store') }}" method="POST">
        @csrf @method($blackOut->id ? 'PATCH' : 'POST')

        <header class="d-flex flex-wrap justify-content-between">
            <h1>
                {{ $blackOut->id ? 'Edit' : 'Create' }} Black-out Dates
            </h1>

            <button class="btn btn-primary" type="submit">
                Save Black-out Dates
            </button>
        </header>

        <hr class="border-white">

        @include('partials.alerts')

        <div class="form-group required">
            <label for="name">Name</label>

            <input id="name" name="name" type="text" value="{{ old('name') ?? $blackOut->name }}" class="form-control bg-dark text-white">

            <small>
                <em class="text-muted">
                    This field is just for reference and doesn't appear on the booking form.
                </em>
            </small>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group required">
                    <label for="starts_on">Starts On</label>

                    <flatpickr
                        id="starts_on"
                        name="starts_on"
                        value="{{ old('starts_on') ?? (!empty($blackOut->starts_on) ? $blackOut->starts_on->format('Y-m-d') : '') }}"
                        class="form-control bg-dark text-white"
                    ></flatpickr>

                    <small>
                        <em class="text-muted">
                            This is the start of the date range that you do not want to be available on the booking form.
                        </em>
                    </small>
                </div>
            </div>

            <div class="col">
                <div class="form-group required">
                    <label for="ends_on">Ends On</label>

                    <flatpickr
                        id="ends_on"
                        name="ends_on"
                        value="{{ old('ends_on') ?? (!empty($blackOut->ends_on) ? $blackOut->ends_on->format('Y-m-d') : '') }}"
                        class="form-control bg-dark text-white"
                    ></flatpickr>

                    <small>
                        <em class="text-muted">
                            This is the end of the date range that you do not want to be available on the booking form.
                        </em>
                    </small>
                </div>
            </div>
        </div>

        <button class="btn btn-primary" type="submit">
            Save Black-out Dates
        </button>
    </form>
@endsection
