@extends('layouts.admin')

@section('content')
    <header class="d-flex flex-wrap justify-content-between">
        <h1>
            Black-out Dates
        </h1>

        <a href="{{ route('admin.black-outs.create') }}">
            <button class="btn btn-primary" type="button">
                Add New Black-out Dates
            </button>
        </a>
    </header>

    @include('partials.alerts')

    <table class="table table-striped bg-dark">
        <thead>
            <tr>
                <th>
                    Name
                </th>

                <th>
                    Starts On
                </th>

                <th>
                    Ends On
                </th>

                <th>
                    <!-- Edit -->
                </th>

                <th>
                    <!-- Delete -->
                </th>
            </tr>
        </thead>

        <tbody>
            @foreach ($blackOuts as $blackOut)
                <tr>
                    <td>
                        {{ $blackOut->name }}
                    </td>

                    <td>
                        {{ $blackOut->starts_on->format('F jS, Y') }}
                    </td>

                    <td>
                        {{ $blackOut->ends_on->format('F jS, Y') }}
                    </td>

                    <td>
                        <a href="{{ route('admin.black-outs.edit', $blackOut) }}" title="Edit Black-out Dates">
                            <button class="btn btn-primary" type="button">
                                Edit
                            </button>
                        </a>
                    </td>

                    <td>
                        <form action="{{ route('admin.black-outs.destroy', $blackOut) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to delete these black-out dates?')) return false;">
                            @csrf @method('DELETE')

                            <button class="btn btn-outline-danger" type="submit" title="Delete Black-out Dates">
                                Delete
                            </button>
                        </form>
                    </td>
                </tr>

                {{ $blackOuts->appends(request()->all())->links() }}
            @endforeach
        </tbody>
    </table>
@endsection
