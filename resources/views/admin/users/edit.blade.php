@extends('layouts.admin')

@section('content')
    <form action="{{ $user->id ? route('admin.users.update', $user) : route('admin.users.store') }}" method="POST">
        @csrf @method($user->id ? 'PATCH' : 'POST')

        <header class="d-flex flex-wrap justify-content-between">
            <h1>{{ $user->id ? 'Edit' : 'Create' }} User</h1>

            <button class="btn btn-primary" type="submit">Save User</button>
        </header>

        <hr class="border-white">

        @include('partials.alerts')

        <div class="form-group required">
            <label for="name">Name</label>

            <input id="name" name="name" type="text" value="{{ old('name') ?? $user->name }}" class="form-control bg-dark text-white">
        </div>

        <div class="form-group required">
            <label for="email">Email</label>

            <input id="email" name="email" type="email" value="{{ old('email') ?? $user->email }}" class="form-control bg-dark text-white">
        </div>

        <div class="form-group">
            <label for="password">Password</label>

            <input id="password" name="password" type="password" class="form-control bg-dark text-white">
        </div>

        <div class="form-group">
            <label for="password_confirmation">Confirm Password</label>

            <input id="password_confirmation" name="password_confirmation" type="password" class="form-control bg-dark text-white">
        </div>

        <div class="form-group">
            <label for="receives_contact_form">Receives Contact Form</label>

            <select id="receives_contact_form" name="receives_contact_form" class="form-control bg-dark text-white">
                <option value="no" {{ $user->receives_contact_form ? '' : 'selected' }}>No</option>

                <option value="yes" {{ $user->receives_contact_form ? 'selected' : '' }}>Yes</option>
            </select>
        </div>

        <button class="btn btn-primary" type="submit">Save User</button>
    </form>
@endsection
