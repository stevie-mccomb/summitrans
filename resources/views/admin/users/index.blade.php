@extends('layouts.admin')

@section('content')
    <header class="d-flex flex-wrap justify-content-between">
        <h1>Users</h1>

        <a href="{{ route('admin.users.create') }}">
            <button class="btn btn-primary" type="button">Create User</button>
        </a>
    </header>

    @include('partials.alerts')

    <table class="table table-striped bg-dark">
        <thead>
            <tr>
                <th>
                    Name
                </th>

                <th>
                    Email
                </th>

                <th></th>

                <th></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>
                        {{ $user->name }}
                    </td>

                    <td>
                        {{ $user->email }}
                    </td>

                    <td>
                        <a href="{{ route('admin.users.edit', $user) }}" title="Edit User">
                            <button class="btn btn-primary" type="button">
                                Edit
                            </button>
                        </a>
                    </td>

                    <td>
                        <form action="{{ route('admin.users.destroy', $user) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to delete this user?')) return false;">
                            @csrf @method('DELETE')

                            <button class="btn btn-outline-danger" type="submit" title="Delete User">
                                Delete
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
