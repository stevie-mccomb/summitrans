<aside class="admin-sidebar">
    <nav class="nav nav-pills flex-column">
        <a class="nav-link {{ ActiveRoute::startsWith('admin.bookings.index') }}" href="{{ route('admin.bookings.index') }}">
            Bookings
        </a>

        <a class="nav-link {{ ActiveRoute::startsWith('admin.black-outs.index') }}" href="{{ route('admin.black-outs.index') }}">
            Black Out Dates
        </a>

        <a class="nav-link {{ ActiveRoute::startsWith('admin.users.index') }}" href="{{ route('admin.users.index') }}">
            Users
        </a>
    </nav>
</aside>
