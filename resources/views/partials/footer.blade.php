<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col py-5">
                <p class="pull-right">
                    All Content Copyright SummiTrans &copy; 2018&ndash;{{ date('Y') }}. All Rights Reserved.
                </p>
            </div>
        </div>
    </div>
</footer>

<unsplash-photo-credits :credits="[
    {
        name: 'Olivia Hutcherson',
        url: 'https://unsplash.com/@ohutcherson?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge',
        thumbnail: '/img/olivia-hutcherson-768718-unsplash-thumbnail.jpg'
    }
]"></unsplash-photo-credits>
