<a name="top" style="position: absolute; top: 0;"></a>

<header class="top-nav">
    <div class="container">
        <div class="d-flex justify-content-end">
            <a href="tel:8012434132">
                <i class="fa fa-phone"></i> (801) 243-4132
            </a>
        </div>
    </div>
</header>

<header class="site-header">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container">
            @if (request()->is('/'))
                <scroll-anchor class="navbar-brand" href="#top">
                    <img src="/img/logo.png" alt="SummiTrans Logo">
                </scroll-anchor>
            @else
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="/img/logo.png" alt="SummiTrans Logo">
                </a>
            @endif

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="False" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        @if (request()->is('/'))
                            <scroll-anchor class="nav-link" href="#top">
                                Home <span class="sr-only">(current)</span>
                            </scroll-anchor>
                        @else
                            <a class="nav-link" href="{{ route('home') }}">
                                Home <span class="sr-only">(current)</span>
                            </a>
                        @endif
                    </li>

                    <li class="nav-item">
                        <scroll-anchor class="nav-link" href="{{ route('home') }}#pricing" :offset="-240">Pricing</scroll-anchor>
                    </li>

                    <li class="nav-item">
                        <scroll-anchor class="nav-link" href="{{ route('home') }}#hours" :offset="-240">Hours</scroll-anchor>
                    </li>

                    <li class="nav-item">
                        <scroll-anchor class="nav-link" href="{{ route('home') }}#location" :offset="-240">Location</scroll-anchor>
                    </li>

                    <li class="nav-item">
                        <scroll-anchor class="nav-link" href="{{ route('home') }}#faqs" :offset="-240">FAQs</scroll-anchor>
                    </li>

                    <li class="nav-item">
                        <scroll-anchor class="nav-link" href="{{ route('home') }}#contact" :offset="-240">Contact</scroll-anchor>
                    </li>

                    <li class="nav-item">
                        <scroll-anchor class="nav-link" href="{{ route('home') }}#booking" :offset="-240">Booking</scroll-anchor>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
