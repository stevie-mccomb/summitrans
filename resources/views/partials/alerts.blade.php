@if ($errors->count())
    <alert type="danger" header="Whoops! It looks like there was a problem with your input:" :messages="{{ json_encode($errors->all()) }}"></alert>
@endif

@if (Session::has('success'))
    <alert type="success" header="Success!" message="{{ Session::get('success') }}"></alert>
@endif

@if (Session::has('info'))
    <alert type="info" header="Notice" message="{{ Session::get('info') }}"></alert>
@endif

@if (Session::has('status'))
    <alert type="info" header="Notice" message="{{ Session::get('status') }}"></alert>
@endif

@if (Session::has('warning'))
    <alert type="warning" header="Warning!" message="{{ Session::get('warning') }}"></alert>
@endif

@if (Session::has('danger'))
    <alert type="danger" header="Warning!" message="{{ Session::get('danger') }}"></alert>
@endif
