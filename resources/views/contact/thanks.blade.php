@extends('layouts.app')

@section('content')
    <article class="contact-thanks py-5">
        <section class="container">
            <header class="pb-3">
                <h1>Thank You</h1>
            </header>

            <p>We have received your message and will be in touch shortly, if required.</p>
        </section>
@endsection
