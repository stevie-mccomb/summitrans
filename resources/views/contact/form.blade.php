@extends('layouts.app')

@section('content')
    <article class="contact-thanks py-5">
        <section class="container">
            <header class="pb-3">
                <h1>Contact Us</h1>
            </header>

            <form action="{{ route('contact.post') }}" method="POST">
                @csrf

                <div class="form-group">
                    <label for="name">Name</label>

                    <input id="name" name="name" type="text" value="{{ old('name') }}" class="form-control bg-dark text-white border-dark" placeholder="Your Name">
                </div>

                <div class="form-group">
                    <label for="Email">Email</label>

                    <input id="email" name="email" type="email" value="{{ old('email') }}" class="form-control bg-dark text-white border-dark" placeholder="Your Email">
                </div>

                <div class="form-group">
                    <label for="phone">Phone</label>

                    <input id="phone" name="phone" type="phone" value="{{ old('phone') }}" class="form-control bg-dark text-white border-dark" placeholder="(555) 555-5555">
                </div>

                <div class="form-group">
                    <label for="message">Message</label>

                    <textarea id="message" name="message" rows="5" class="form-control bg-dark text-white border-dark">{{ old('message') }}</textarea>
                </div>

                <button class="btn btn-outline-light" type="submit">
                    Send Message <i class="fa fa-angle-right"></i>
                </button>
            </form>
        </section>
@endsection
