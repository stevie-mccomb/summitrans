@extends('layouts.email')

@section('content')
    <center>
        <table class="container">
            <thead>
                <tr>
                    <th colspan="2">
                        <h2>SummiTrans</h2>

                        @if ($booking->paid)
                            <h1>Ride Receipt</h1>

                            <p>Your ride with SummiTrans has been completed and paid for. Please review your ride details and payment confirmation below.</p>
                        @else
                            <h1>Ride Updated</h1>

                            <p>The below ride has been updated and confirmed. Please review the details of the ride below.</p>
                        @endif
                    </th>
                </tr>
            </thead>

            <tbody>
                @if ($booking->paid)
                    <tr>
                        <th>Total</th>

                        <td>&#36;{{ number_format(round($booking->charge->amount / 100, 2), 2) }}</td>
                    </tr>

                    <tr>
                        <th>Payment Description</th>

                        <td>{{ $booking->charge->description }}</td>
                    </tr>
                @endif

                <tr>
                    <th>View Ride</th>

                    <td>
                        <a target="_blank" href="{{ route('bookings.show', ['uid' => $booking->uid ]) }}">View Ride on Website &gt;</a>
                    </td>
                </tr>

                <tr>
                    <th>Reference ID</th>

                    <td>{{ $booking->uid }}</td>
                </tr>

                <tr>
                    <th>Name</th>

                    <td>{{ $booking->full_name }}</td>
                </tr>

                <tr>
                    <th>Pickup Time</th>

                    <td>{{ $booking->pickup_at->format('g:ia T \o\n F jS, Y') }}</td>
                </tr>

                <tr>
                    <th>Pickup Location</th>

                    <td>{{ $booking->origin_full_address }}</td>
                </tr>

                <tr>
                    <th>Pickup Navigation URL</th>

                    <td>
                        <a target="_blank" href="{{ $booking->origin_url }}">
                            {{ $booking->origin_url }}
                        </a>
                    </td>
                </tr>

                <tr>
                    <th>Destination Location</th>

                    <td>{{ $booking->destination_full_address }}</td>
                </tr>

                <tr>
                    <th>Destination Navigation URL</th>

                    <td>
                        <a target="_blank" href="{{ $booking->destination_url }}">
                            {{ $booking->destination_url }}
                        </a>
                    </td>
                </tr>

                <tr>
                    <th>Number of Passengers</th>

                    <td>{{ $booking->passengers }} passenger{{ $booking->passengers === 1 ? '' : 's' }}</td>
                </tr>

                <tr>
                    <th>Airport Pickup</th>

                    <td>{{ $booking->airport_pickup ? 'Yes' : 'No' }}</td>
                </tr>

                @if ($booking->airport_pickup)
                    <tr>
                        <th>Airline</th>

                        <td>{{ $booking->airline }}</td>
                    </tr>

                    <tr>
                        <th>Flight Number</th>

                        <td>{{ $booking->flight_number }}</td>
                    </tr>

                    <tr>
                        <th>Departing City</th>

                        <td>{{ $booking->departing_city }}</td>
                    </tr>
                @endif

                <tr>
                    <th>Notes/Special Requests</th>

                    <td>{{ $booking->notes }}</td>
                </tr>

                <tr>
                    <th>Created At</th>

                    <td>{{ $booking->created_at->format('g:ia T \o\n F jS, Y') }}</td>
                </tr>

                <tr>
                    <th>Updated At</th>

                    <td>{{ $booking->updated_at->format('g:ia T \o\n F jS, Y') }}</td>
                </tr>
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="2">
                        <p>If there are any issues with the content above, please <a href="mailto:stevie.mccomb@gmail.com">let us know</a>!</p>
                    </td>
                </tr>
            </tfoot>
        </table>
    </center>
@endsection
