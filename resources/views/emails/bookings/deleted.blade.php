@extends('layouts.email')

@section('content')
    <center>
        <table class="container">
            <thead>
                <tr>
                    <th colspan="2">
                        <h2>SummiTrans</h2>

                        <h1>Booking Cancelled</h1>

                        <p>The below booking has been cancelled. If you feel that this is a mistake, please <a href="{{ url('home') }}#contact">contact us</a>.</p>
                    </th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <th>Reference ID</th>

                    <td>{{ $booking->uid }}</td>
                </tr>

                <tr>
                    <th>Name</th>

                    <td>{{ $booking->full_name }}</td>
                </tr>

                <tr>
                    <th>Pickup Time</th>

                    <td>{{ $booking->pickup_at->format('g:ia T \o\n F jS, Y') }}</td>
                </tr>

                <tr>
                    <th>Pickup Location</th>

                    <td>{{ $booking->origin_full_address }}</td>
                </tr>

                <tr>
                    <th>Destination Location</th>

                    <td>{{ $booking->destination_full_address }}</td>
                </tr>

                <tr>
                    <th>Number of Passengers</th>

                    <td>{{ $booking->passengers }} passenger{{ $booking->passengers === 1 ? '' : 's' }}</td>
                </tr>

                <tr>
                    <th>Airport Pickup</th>

                    <td>{{ $booking->airport_pickup ? 'Yes' : 'No' }}</td>
                </tr>

                @if ($booking->airport_pickup)
                    <tr>
                        <th>Airline</th>

                        <td>{{ $booking->airline }}</td>
                    </tr>

                    <tr>
                        <th>Flight Number</th>

                        <td>{{ $booking->flight_number }}</td>
                    </tr>

                    <tr>
                        <th>Departing City</th>

                        <td>{{ $booking->departing_city }}</td>
                    </tr>
                @endif

                <tr>
                    <th>Created At</th>

                    <td>{{ $booking->created_at->format('g:ia T \o\n F jS, Y') }}</td>
                </tr>

                <tr>
                    <th>Cancelled At</th>

                    <td>{{ $booking->deleted_at->format('g:ia T \o\n F jS, Y') }}</td>
                </tr>
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="2">
                        <p>If there are any issues with the content above, please <a href="mailto:stevie.mccomb@gmail.com">contact support</a>.</p>
                    </td>
                </tr>
            </tfoot>
        </table>
    </center>
@endsection
