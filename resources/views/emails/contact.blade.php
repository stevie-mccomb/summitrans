@extends('layouts.email')

@section('content')
    <center>
        <table class="container">
            <thead>
                <tr>
                    <th colspan="2">
                        <h2>SummiTrans</h2>

                        <h1>Contact Form</h1>
                    </th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <th>Name</th>

                    <td>{{ $data->name }}</td>
                </tr>

                <tr>
                    <th>Email</th>

                    <td>{{ $data->email }}</td>
                </tr>

                <tr>
                    <th>Phone</th>

                    <td>{{ $data->phone }}</td>
                </tr>

                <tr>
                    <th>Message</th>

                    <td>{{ $data->content }}</td>
                </tr>
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="2">
                        <p>If there are any issues with the content above, please <a href="mailto:stevie.mccomb@gmail.com">contact support</a>.</p>
                    </td>
                </tr>
            </tfoot>
        </table>
    </center>
@endsection
