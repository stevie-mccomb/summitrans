@include('partials.start')
@include('partials.header')
    <main>
        <div class="container py-5">
            <div class="row">
                <div class="col-auto">
                    @include('admin.sidebar')
                </div>

                <div class="col">
                    @yield('content')
                </div>
            </div>
        </div>
    </main>
@include('partials.footer')
@include('partials.end')
