<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>SummiTrans{{ !empty($subject) ? " - {$subject}" : '' }}</title>

    <style type="text/css">
        html, body {
            color: #666;
            font-size: 16px;
            font-family: 'Open Sans', Helvetica, Calibri, Arial, sans-serif;
            background-color: #eee;
        }

        table {
            border-collapse: collapse;
        }

        th, td {
            padding: 16px;
            background-color: #fff;
            border: 1px solid #ccc;
        }

        .container {
            margin: 0 auto;
            background-color: #fff;
        }

        .container thead th {
            text-align: center;
        }

        .container thead th h1, .container thead th h2 {
            color: #333;
        }

        .container tbody th {
            text-align: left;
        }
    </style>
</head>
<body>
    @yield('content')
</body>
</html>
