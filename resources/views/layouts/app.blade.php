@include('partials.start')
@include('partials.header')
    <main>
        @yield('content')
    </main>
@include('partials.footer')
@include('partials.end')
