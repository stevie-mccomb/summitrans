<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('bookings', 'BookingController@store')->name('bookings.store');
Route::get('bookings/{uid}', 'BookingController@show')->name('bookings.show');
Route::get('bookings/{uid}/edit', 'BookingController@edit')->name('bookings.edit');
Route::delete('bookings/{uid}', 'BookingController@destroy')->name('bookings.destroy');
Route::post('bookings/{uid}/update', 'BookingController@update')->name('bookings.update');
Route::post('bookings/{uid}/stop', 'BookingController@stop')->name('bookings.stop');
Route::post('bookings/{uid}/resume', 'BookingController@resume')->name('bookings.resume');
Route::get('bookings/{uid}/pay', 'BookingController@getPay')->name('bookings.pay');
Route::post('bookings/{uid}/pay', 'BookingController@postPay')->name('bookings.pay.post');

Route::get('contact', 'ContactController@form')->name('contact');
Route::post('contact', 'ContactController@send')->name('contact.post');
Route::get('contact/thanks', 'ContactController@thanks')->name('contact.thanks');

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::redirect('/', '/admin/bookings');

    Route::patch('bookings/{booking}/start', 'BookingController@start')->name('bookings.start');
    Route::patch('bookings/{booking}/restore', 'BookingController@restore')->name('bookings.restore');
    Route::get('bookings/{booking}/emails/notification/resend', 'BookingController@resendNotificationEmail')->name('bookings.emails.notification.resend');
    Route::resource('bookings', 'BookingController')->except(['create', 'show']);

    Route::get('black-outs', 'BlackOutController@index')->name('black-outs.index');
    Route::get('black-outs/create', 'BlackOutController@create')->name('black-outs.create');
    Route::post('black-outs', 'BlackOutController@store')->name('black-outs.store');
    Route::get('black-outs/{blackOut}/edit', 'BlackOutController@edit')->name('black-outs.edit');
    Route::patch('black-outs/{blackOut}', 'BlackOutController@update')->name('black-outs.update');
    Route::delete('black-outs/{blackOut}', 'BlackOutController@destroy')->name('black-outs.destroy');

    Route::get('users', 'UserController@index')->name('users.index');
    Route::get('users/create', 'UserController@create')->name('users.create');
    Route::post('users', 'UserController@store')->name('users.store');
    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');
    Route::patch('users/{user}', 'UserController@update')->name('users.update');
    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy');
});
