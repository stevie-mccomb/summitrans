<?php

namespace App\Rules;

use Exception;
use Illuminate\Contracts\Validation\Rule;

class Recaptcha implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => [
                'secret' => config('services.google.recaptcha.secret'),
                'response' => $value,
            ],
            CURLOPT_RETURNTRANSFER => true,
        ]);
        $response = curl_exec($ch);
        $error = curl_error($ch);

        if (!empty($error)) return false;
        if (!empty($response) && $this->isJson($response)) $response = json_decode($response);

        return !empty($response) && !empty($response->success) && $response->success == true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Recaptcha check failed, please try again.';
    }

    /**
     * Return whether or not the given string is in JSON format.
     *
     * @param  string  $str
     * @return boolean
     */
    private function isJson($str)
    {
        try {
            json_decode($str);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
