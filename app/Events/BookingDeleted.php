<?php

namespace App\Events;

use Mail;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Mail\BookingDeleted as BookingDeletedEmail;
use App\Models\Booking;
use App\Models\User;

class BookingDeleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $recipients = User::select(['users.name', 'users.email'])->get()->toArray();

        $recipients[] = [
            'name' => $booking->full_name,
            'email' => $booking->email,
        ];

        Mail::to($recipients)->queue(new BookingDeletedEmail($booking));
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
