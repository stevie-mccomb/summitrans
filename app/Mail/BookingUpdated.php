<?php

namespace App\Mail;

use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingUpdated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The booking that was updated.
     *
     * @var \App\Models\Booking
     */
    public $booking;

    /**
     * The email's subject (for the template title).
     *
     * @var string
     */
    public $subject = 'SummiTrans - Ride Updated';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;

        if ($this->booking->paid) $this->subject = 'SummiTrans - Ride Receipt';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.updated');
    }
}
