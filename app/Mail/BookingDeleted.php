<?php

namespace App\Mail;

use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingDeleted extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The booking that was deleted.
     *
     * @var \App\Models\Booking
     */
    public $booking;

    /**
     * The email's subject (for the template title).
     *
     * @var string
     */
    public $subject = 'SummiTrans - Booking Cancelled';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.deleted');
    }
}
