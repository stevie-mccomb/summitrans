<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The data to display in the email template.
     *
     * @var array
     */
    public $data;

    /**
     * The email's subject (for the template title).
     *
     * @var string
     */
    public $subject = 'SummiTrans - Contact Form';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = (Object) $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $recipients = User::where('receives_contact_form', 1)->get();

        return $this->to($recipients)->view('emails.contact');
    }
}
