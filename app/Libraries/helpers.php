<?php

if (!function_exists('today')) {
    function today()
    {
        return new \Carbon\Carbon('today');
    }
}

if (!function_exists('tomorrow')) {
    function tomorrow()
    {
        return new \Carbon\Carbon('tomorrow');
    }
}

if (!function_exists('dayAfterTomorrow')) {
    function dayAfterTomorrow()
    {
        return new \Carbon\Carbon('tomorrow + 1 day');
    }
}
