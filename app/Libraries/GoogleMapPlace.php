<?php

namespace App\Libraries;

class GoogleMapPlace
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;

        foreach ($this->data['address_components'] as &$component) {
            if (!is_array($component)) $component = (Array) $component;
        }
    }

    public function __get($key)
    {
        if (isset($this->data[$key])) return $this->data[$key];

        foreach ($this->data['address_components'] as $component) {
            foreach ($component['types'] as $type) {
                if ($type === $key) return $component;
            }
        }

        return null;
    }
}
