<?php

namespace App\Http\Controllers;

use App\Libraries\GoogleMapPlace;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class BookingController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email',
            'phone' => 'required|string|min:7|max:32',
            'origin' => 'required|array',
            'origin.address_components' => 'required|array',
            'origin.formatted_address' => 'required|string',
            'destination' => 'required|array',
            'destination.address_components' => 'required|array',
            'destination.formatted_address' => 'required|string',
            'number_of_passengers' => 'required|integer|min:1|max:7',
            'airport_pickup' => 'required|in:Yes,No',
            'airline' => 'required_if:airport_pickup,Yes|nullable|string|max:255',
            'flight_number' => 'required_if:airport_pickup,Yes|nullable|string|max:32',
            'departing_city' => 'required_if:airport_pickup,Yes|nullable|string|max:255',
            'datetime' => 'required|date|after:now',
            'notes' => 'nullable|string',
        ]);

        $origin = new GoogleMapPlace($request->input('origin'));
        $destination = new GoogleMapPlace($request->input('destination'));

        $booking = new Booking;
        $booking->first_name = $request->input('first_name');
        $booking->last_name = $request->input('last_name');
        $booking->full_name = $request->input('first_name') . ' ' . $request->input('last_name');
        $booking->email = $request->input('email');
        $booking->phone = $request->input('phone');
        $booking->origin_full_address = $request->input('origin.formatted_address');
        $booking->origin_address1 = ($origin->street_number['long_name'] ?? '') . ' ' . ($origin->route['long_name'] ?? '');
        $booking->origin_city = $origin->locality['long_name'];
        $booking->origin_state = $origin->administrative_area_level_1['long_name'];
        $booking->origin_zip = $origin->postal_code['long_name'];
        $booking->origin_url = $origin->url;
        $booking->destination_full_address = $request->input('destination.formatted_address');
        $booking->destination_address1 = ($destination->street_number['long_name'] ?? '') . ' ' . ($destination->route['long_name'] ?? '');
        $booking->destination_city = $destination->locality['long_name'];
        $booking->destination_state = $destination->administrative_area_level_1['long_name'];
        $booking->destination_zip = ($destination->postal_code['long_name'] ?? 'Unknown');
        $booking->destination_url = $destination->url;
        $booking->passengers = $request->input('number_of_passengers');
        $booking->pickup_at = (new Carbon($request->input('datetime')))->format('Y-m-d H:i:s');
        $booking->notes = $request->input('notes');
        $booking->airport_pickup = $request->input('airport_pickup') === 'Yes';
        $booking->airline = $request->input('airline') ?? null;
        $booking->flight_number = $request->input('flight_number') ?? null;
        $booking->departing_city = $request->input('departing_city') ?? null;
        $booking->save();

        return response()->json([
            'status' => 200,
            'booking' => $booking,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $uid
     * @return \Illuminate\Http\Response
     */
    public function show($uid)
    {
        $data['booking'] = Booking::withTrashed()->where('uid', $uid)->firstOrFail();

        return view('bookings.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $uid
     * @return \Illuminate\Http\Response
     */
    public function edit($uid)
    {
        $data['booking'] = Booking::where('uid', $uid)->firstOrFail();

        return view('bookings.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string                    $uid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uid)
    {
        $booking = Booking::where('uid', $uid)->firstOrFail();

        $this->validate($request, [
            'stripe_token' => 'required|string',
            'stripe_client_ip' => 'required|ip',
            'stripe_email' => 'required|email',
        ]);

        Stripe::setApiKey(config('services.stripe.secret'));

        $customer = Customer::create([
            'source' => $request->input('stripe_token'),
            'email' => $request->input('stripe_email'),
        ]);

        $booking->customer_id = $customer->id;
        $booking->save();

        return redirect(route('bookings.show', [ 'uid' => $booking->uid ]))->with('success', 'Booking successfully confirmed. Please check your email for a confirmation email. If you do not receive it in a timely manner, please contact us.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $uid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid)
    {
        $booking = Booking::where('uid', $uid)->firstOrFail();

        if ($booking->paid) return abort(403);

        $booking->delete();

        return redirect(route('bookings.show', [ 'uid' => $booking->uid ]))->with('success', 'Ride successfully cancelled.');
    }

    /**
     * Stop the given ride's time tracking and forward the user to the payment page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string                    $uid
     * @return \Illuminate\Http\Response
     */
    public function stop(Request $request, $uid)
    {
        $this->middleware('auth');

        if (empty($booking->stopped_at)) {
            $booking = Booking::whereNotNull('customer_id')->where('uid', $uid)->firstOrFail();
            $booking->stopped_at = now();
            $booking->save();
        }

        return redirect(route('bookings.pay', [ 'uid' => $booking->uid ]));
    }

    /**
     * Resume the given ride's time tracking and redirect to the booking status page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string                    $uid
     * @return \Illuminate\Http\Response
     */
    public function resume(Request $request, $uid)
    {
        $this->middleware('auth');

        if (!empty($booking->stopped_at)) {
            $booking = Booking::whereNotNull('customer_id')->where('uid', $uid)->firstOrFail();
            $booking->stopped_at = null;
            $booking->save();
        }

        return redirect(route('bookings.show', [ 'uid' => $booking->uid ]))->with('success', 'Clock successfully resumed for this ride.');
    }

    /**
     * Display the payment form.
     *
     * @param  string  $uid
     * @return \Illuminate\Http\Response
     */
    public function getPay($uid)
    {
        $this->middleware('auth');

        $data['booking'] = Booking::whereNotNull('customer_id')->where('uid', $uid)->firstOrFail();

        return view('bookings.pay', $data);
    }

    /**
     * Submit payment for the given booking.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string                    $uid
     * @return \Illuminate\Http\Response
     */
    public function postPay(Request $request, $uid)
    {
        $booking = Booking::whereNotNull('customer_id')->where('uid', $uid)->firstOrFail();

        $this->validate($request, [
            'base_amount' => 'required|numeric',
            'tip_type' => 'required|string|in:10%,15%,20%,Custom',
            'custom_tip_amount' => 'nullable|string',
        ]);

        // $seconds = $booking->stopped_at->diffInSeconds($booking->started_at);
        // $hours = $seconds / 60 / 60; // rate is in pennies per hour so the ride time needs to be converted from seconds to hours
        // $base = round($hours * $booking->rate); // can't have partial pennies, so needs to round to nearest penny
        $base = round(((Float) $request->base_amount) * 100); // Convert the given dollars to pennies.

        $tipType = $request->input('tip_type');

        switch ($tipType) {
            case '10%':
                $tipped = $base + ($base * 0.10);
                break;

            case '15%':
                $tipped = $base + ($base * 0.15);
                break;

            case '20%':
                $tipped = $base + ($base * 0.20);
                break;

            case 'Custom':
                $custom = (Float) $request->input('custom_tip_amount');
                $tipped = $base + ($custom * 100);
                break;

            default:
                $tipped = $base;
                break;
        }

        // Convert to integers in case there's a floating point value.
        $base = (Int) $base;
        $tipped = (Int) $tipped;

        $baseFormatted = '$' . number_format(round($base / 100, 2), 2);
        $tippedFormatted = '$' . number_format(round($tipped / 100, 2), 2);

        Stripe::setApiKey(config('services.stripe.secret'));

        $charge = Charge::create([
            'amount' => $tipped,
            'currency' => 'usd',
            'customer' => $booking->customer_id,
            'description' => "Ride for {$booking->email} - Base Amount: {$baseFormatted} - Tip Type: {$tipType} - After Tip: {$tippedFormatted}",
            'metadata' => [
                'base' => $base,
                'after_tip' => $tipped,
                'base_formatted' => $baseFormatted,
                'after_tip_formatted' => $tippedFormatted,
            ],
        ]);

        $booking->charge_id = $charge->id;
        $booking->save();

        return redirect(route('bookings.show', [ 'uid' => $booking->uid ]))->with('success', 'Ride successfully paid for. Please check your email for your receipt.');
    }
}
