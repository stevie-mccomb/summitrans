<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;

use App\Mail\Contact as ContactMessage;
use App\Rules\Recaptcha;

class ContactController extends Controller
{
    /**
     * Display the standalone contact form.
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        return view('contact.form');
    }

    /**
     * Send the given message to the contact email.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
            'real_email' => 'required|email|max:255',
            'phone' => 'required|string|max:32',
            'message' => 'required|string',
            'grecaptcha' => ['required', new Recaptcha],
        ]);

        // Honeypot
        if ($request->filled('email')) return redirect(route('contact.thanks'));

        $data['name'] = $request->input('name');
        $data['email'] = $request->input('real_email');
        $data['phone'] = $request->input('phone');
        $data['content'] = $request->input('message');

        Mail::queue(new ContactMessage($data));

        return redirect(route('contact.thanks'));
    }

    /**
     * Display the thank you page.
     *
     * @return \Illuminate\Http\Response
     */
    public function thanks()
    {
        return view('contact.thanks');
    }
}
