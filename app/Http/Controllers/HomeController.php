<?php

namespace App\Http\Controllers;

use App\Models\BlackOut;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['blackOutDates'] = BlackOut::forView();

        return view('home', $data);
    }
}
