<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlackOut;
use Illuminate\Http\Request;

class BlackOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illumiante\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = BlackOut::query();

        if ($request->filled('search')) {
            $query->where('name', 'like', "%{$request->input('search')}%");
        }

        $data['blackOuts'] = $query->paginate(25);

        return view('admin.black-outs.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->edit(new BlackOut);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new BlackOut);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlackOut  $blackOut
     * @return \Illuminate\Http\Response
     */
    public function edit(BlackOut $blackOut)
    {
        return view('admin.black-outs.edit', [ 'blackOut' => $blackOut, ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlackOut      $blackOut
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlackOut $blackOut)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'starts_on' => 'required|date_format:Y-m-d|before:ends_on',
            'ends_on' => 'required|date_format:Y-m-d|after:starts_on',
        ]);

        $blackOut->fill($request->only('name', 'starts_on', 'ends_on'));
        $blackOut->save();

        return redirect(route('admin.black-outs.index'))->with('success', 'Black-out dates successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlackOut  $blackOut
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlackOut $blackOut)
    {
        $blackOut->delete();

        return redirect(route('admin.black-outs.index'))->with('success', 'Black-out dates successfully deleted.');
    }
}
