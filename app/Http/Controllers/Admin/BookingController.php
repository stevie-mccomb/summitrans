<?php

namespace App\Http\Controllers\Admin;

use Mail;
use App\Http\Controllers\Controller;
use App\Libraries\GoogleMapPlace;
use App\Mail\BookingUpdated;
use App\Models\Booking;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Booking::withTrashed()->orderBy('pickup_at', 'desc');

        if ($request->filled('search')) {
            $query->where(function ($query) {
                $value = "%{$request->input('search')}%";

                $query->where('full_name', 'like', $value);
            });
        }

        $data['bookings'] = $query->paginate(25);

        return view('admin.bookings.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->edit(new Booking);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new Booking);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        $data['booking'] = $booking;

        return view('admin.bookings.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Booking       $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        $rules = [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email',
            'phone' => 'required|string|min:7|max:32',
            'airport_pickup' => 'required|in:true,false',
            'airline' => 'required_if:airport_pickup,true|nullable|string|max:255',
            'flight_number' => 'required_if:airport_pickup,true|nullable|string|max:32',
            'departing_city' => 'required_if:airport_pickup,true|nullable|string|max:255',
            'number_of_passengers' => 'required|integer|min:1|max:7',
            'datetime' => 'date|after:now',
            'notes' => 'nullable|string',
        ];

        if (empty($booking->id)) {
            $rules['pickup_location'] = 'required|json';
            $rules['destination_location'] = 'required|json';
        } else {
            $rules['pickup_location'] = 'nullable';
            $rules['destination_location'] = 'nullable';
        }

        $this->validate($request, $rules);

        $pickupLocationData = (Array) json_decode($request->input('pickup_location'));
        $destinationLocationData = (Array) json_decode($request->input('destination_location'));

        if (!empty($pickupLocationData)) $origin = new GoogleMapPlace($pickupLocationData);
        if (!empty($destinationLocationData)) $destination = new GoogleMapPlace($destinationLocationData);

        $booking->first_name = $request->input('first_name');
        $booking->last_name = $request->input('last_name');
        $booking->full_name = $request->input('first_name') . ' ' . $request->input('last_name');
        $booking->email = $request->input('email');
        $booking->phone = $request->input('phone');
        if (!empty($origin)) {
            $booking->origin_full_address = $origin->formatted_address;
            $booking->origin_address1 = $origin->street_number['long_name'] . ' ' . $origin->route['long_name'];
            $booking->origin_city = $origin->locality['long_name'];
            $booking->origin_state = $origin->administrative_area_level_1['long_name'];
            $booking->origin_zip = $origin->postal_code['long_name'];
            $booking->origin_url = $origin->url;
        }
        if (!empty($destination)) {
            $booking->destination_full_address = $destination->formatted_address;
            $booking->destination_address1 = $destination->street_number['long_name'] . ' ' . $destination->route['long_name'];
            $booking->destination_city = $destination->locality['long_name'];
            $booking->destination_state = $destination->administrative_area_level_1['long_name'];
            $booking->destination_zip = $destination->postal_code['long_name'];
            $booking->destination_url = $destination->url;
        }
        $booking->passengers = $request->input('number_of_passengers');
        $booking->pickup_at = (new Carbon($request->input('datetime')))->format('Y-m-d H:i:s');
        $booking->notes = $request->input('notes');
        $booking->airport_pickup = $request->input('airport_pickup') === 'true';
        $booking->airline = $request->input('airline') ?? null;
        $booking->flight_number = $request->input('flight_number') ?? null;
        $booking->departing_city = $request->input('departing_city') ?? null;
        $booking->save();

        return redirect(route('admin.bookings.edit', $booking))->with('success', 'Booking successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $booking->delete();

        return redirect()->back()->with('success', 'Booking successfully cancelled. An email notification has been sent out to the individual that created the booking.');
    }

    /**
     * Un-delete the resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $booking = Booking::withTrashed()->where('id', $id)->firstOrFail();

        $booking->restore();

        return redirect()->back()->with('success', 'Booking successfully restored. An email notification has been sent out to the individual that created the booking.');
    }

    /**
     * Start the clock for tracking the given booking's ride.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function start(Booking $booking)
    {
        $booking->started_at = now();
        $booking->save();

        return redirect(route('bookings.show', [ 'uid' => $booking->uid ]))->with('success', 'The ride has begun.');
    }

    /**
     * Re-sends the admin notification email w/ the booking details.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function resendNotificationEmail(Booking $booking)
    {
        $recipients = User::select(['users.name', 'users.email'])->get()->toArray();

        Mail::to($recipients)->queue(new BookingUpdated($booking));

        return redirect()->back()->with('success', 'Email successfully queued for sending.');
    }
}
