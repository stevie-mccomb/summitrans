<?php

namespace App\Models;

use App\Events\BookingCreating;
use App\Events\BookingDeleted;
use App\Events\BookingUpdated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Stripe\Charge;
use Stripe\Stripe;

class Booking extends Model
{
    use SoftDeletes;

    /**
     * Ther attributes that should always be mutated and appended to this model's query results.
     *
     * @var array
     */
    protected $appends = [
        'base_amount',
    ];

    /**
     * The attributes that should automatically be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'pickup_at',
        'started_at',
        'stopped_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'creating' => BookingCreating::class,
        'deleted' => BookingDeleted::class,
        'updated' => BookingUpdated::class,
    ];

    /**
     * The attributes that are mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'full_name',
        'origin_full_address',
        'origin_address1',
        'origin_address2',
        'origin_city',
        'origin_state',
        'origin_zip',
        'origin_url',
        'destination_full_address',
        'destination_address1',
        'destination_address2',
        'destination_city',
        'destination_state',
        'destination_zip',
        'destination_url',
        'passengers',
        'airport_pickup',
        'airline',
        'flight_number',
        'departing_city',
        'notes',
        'pickup_at',
        'started_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Generate a (hopefully) unique 16 character identifier string.
     *
     * @return string
     */
    public function generateUID()
    {
        $uid = '';
        $chars = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ');

        while (strlen($uid) < 8) $uid .= $chars[floor(rand(0, count($chars) - 1))];

        return $uid;
    }

    /**
     * Return whether or not this booking has been abandoned.
     *
     * @return boolean
     */
    public function getAbandonedAttribute()
    {
        return !empty($this->id) && empty($this->customer_id);
    }

    /**
     * Return the starting base price for most rides ($155.00).
     *
     * @return integer
     */
    public function getBaseAmountAttribute()
    {
        return 14500;
    }

    /**
     * Return the Stripe charge for this booking.
     *
     * @return \Stripe\Charge|boolean
     */
    public function getChargeAttribute()
    {
        if (empty($this->charge_id)) return false;

        Stripe::setApiKey(config('services.stripe.secret'));

        return Charge::retrieve($this->charge_id);
    }

    /**
     * Return whether or not this booking has been paid for.
     *
     * @return boolean
     */
    public function getPaidAttribute()
    {
        return !empty($this->charge_id);
    }

    /**
     * Return the rate that this booking charges in pennies per hour.
     *
     * @return integer
     */
    public function getRateAttribute()
    {
        return 5000;
    }

    /**
     * Return whether or not this ride has been started.
     *
     * @return boolean
     */
    public function getStartedAttribute()
    {
        return !empty($this->started_at) && $this->started_at <= now() && !$this->paid;
    }

    /**
     * Return whether or not this ride is stopped and awaiting payment.
     *
     * @return boolean
     */
    public function getStoppedAttribute()
    {
        return $this->started && !empty($this->stopped_at) && !$this->paid;
    }
}
