<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlackOut extends Model
{
    /**
     * The attributes that should be cast to dates.
     *
     * @var array
     */
    protected $dates = [
        'starts_on',
        'ends_on',
    ];

    /**
     * The attributes that are mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'starts_on',
        'ends_on',
    ];

    /**
     * Return future black-out dates in a format that the Flatpickr component can consume.
     *
     * @return array
     */
    public static function forView()
    {
        return self::query()
            ->where('starts_on', '>=', now())
            ->get()
            ->map(function ($item, $key) {
                return [
                    'from' => $item->starts_on->format('Y-m-d'),
                    'to' => $item->ends_on->format('Y-m-d'),
                ];
            })
            ->toJson();
    }
}
