const mix = require('laravel-mix');

mix.options({ procesCssUrls: false });

mix.js('resources/js/app.js', 'public/js').vue();
mix.sass('resources/sass/app.scss', 'public/css');

if (mix.inProduction()) {
    mix.copy('node_modules/font-awesome/fonts', 'public/fonts');

    mix.version();
}
