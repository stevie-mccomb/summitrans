<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid', 8)->unique();
            $table->string('charge_id')->nullable();
            $table->string('customer_id')->nullable();

            $table->string('first_name');
            $table->string('last_name');
            $table->string('full_name');
            $table->string('email');
            $table->string('phone');

            $table->string('origin_full_address');
            $table->string('origin_address1');
            $table->string('origin_city');
            $table->string('origin_state');
            $table->string('origin_zip');
            $table->string('origin_url');

            $table->string('destination_full_address');
            $table->string('destination_address1');
            $table->string('destination_city');
            $table->string('destination_state');
            $table->string('destination_zip');
            $table->string('destination_url');

            $table->text('notes')->nullable();

            $table->boolean('airport_pickup')->default(0);
            $table->string('airline')->nullable();
            $table->string('flight_number', 32)->nullable();
            $table->string('departing_city')->nullable();

            $table->datetime('pickup_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->datetime('started_at')->nullable();
            $table->datetime('stopped_at')->nullable();
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
